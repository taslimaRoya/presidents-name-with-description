//
//  customViewController.h
//  finalCustomcell
//
//  Created by Admin on 9/24/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface customViewController : UIViewController

@property NSString *name;
@property NSString *imageString;
@property (weak, nonatomic) IBOutlet UILabel *nameOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *imageOutlet;

@end

NS_ASSUME_NONNULL_END
