//
//  main.m
//  finalCustomcell
//
//  Created by Taslima Roya on 9/19/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
