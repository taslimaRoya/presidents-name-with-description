//
//  customTableViewCell.h
//  finalCustomcell
//
//  Created by Taslima Roya on 9/19/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UILabel*label1;
@property(nonatomic,weak)IBOutlet UILabel*label2;
@property(nonatomic,weak)IBOutlet UIImageView*image;
@end
