//
//  customViewController.m
//  finalCustomcell
//
//  Created by Admin on 9/24/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import "customViewController.h"

@interface customViewController ()

@end

@implementation customViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _nameOutlet.text = _name;
    _imageOutlet.image = [UIImage imageNamed:_imageString];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
