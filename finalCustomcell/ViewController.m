//
//  ViewController.m
//  finalCustomcell
//
//  Created by Taslima Roya on 9/19/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import "ViewController.h"
#import "customTableViewCell.h"
#import "customViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    NSArray *tableData;
    NSArray*presidentImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    tableData = [NSArray arrayWithObjects:@"sheikhHasina(BANGLADESH)",@"justin trudeau(CANADA)",@"Donald Trump(USA)",@"Salman bin Abdulaziz Al Saud(SAUDI)",@"Xi Jinping(CHINA)",@"Vladimir Putin(RUSSIA)",@"Shinzō Abe(JAPAN)", @"Sergio Mattarella(ITALY)",@"NarendraModi(INDIA)",@"Michel Temer(BRAZIL)",@"Mauricio Macri(ARGENTINA)",@"Malcolm Turnbull(AUSTRALIA)",@"Lars Løkke Rasmussen(DENMARK)",@"Kim Jong-un(KOREA)",@"Frank-Walter Steinmeier(GERMANY)",@"Emmanuel Macron(FRANCE)",@"Charles Michel(BELGIUM)",@"Bidhya Devi Bhandari(NEPAL)",@"Bhumibol Adulyadej(THAILAND)",@"Ashraf Ghani(AFGHANISTAN)",@"Arif Alvi(PAKISTAN)",@"Alain Berset(SWITZERLAND)",nil];
    presidentImage=[NSArray arrayWithObjects:@"sheikhHasina.jpg",@"justin trudeau.jpg",@"Donald Trump.jpg",@"Salman.jpg",@"Xi Jinping .jpg",@"Vladimir Putin.jpg",@"Shinzō Abe.jpg", @"Sergio Mattarella(italy).jpg",@"NarendraModi.jpg",@"Michel Temer(braziil).jpg",@"Mauricio Macri(Argentina).jpg",@"Malcolm Turnbull(australia).jpg",@"Lars Løkke Rasmussen(denmark).jpg",@"Kim Jong-un.jpg",@"Frank-Walter Steinmeier.jpg",@"Emmanuel Macron(france).jpg",@"Charles Michel.jpg",@"Bidhya Devi Bhandari(NEPAL).jpg",@"Bhumibol Adulyadej(thailand).jpg",@"Ashraf Ghani.jpg",@"Arif Alvi.jpg",@"Alain Berset .jpg",nil];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    
    //customTableViewCell *mycell = (customcTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    customTableViewCell*mycell=(customTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (mycell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil];
        mycell = [nib objectAtIndex:0];
    }
    
    mycell.label1.text = [tableData objectAtIndex:indexPath.row];
    
    mycell.image.image=[UIImage imageNamed:[presidentImage objectAtIndex:indexPath.row]];
    mycell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    
    return mycell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 79;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    customViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"newCustomView"];
    vc.name = tableData[indexPath.row];
    vc.imageString = presidentImage[indexPath.row];
    
    [[self navigationController] pushViewController:vc animated:true];
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    
//    NSIndexPath*path=[self.table indexPathForSelectedRow];
////    customViewController *vc;
////    vc=[segue destinationViewController];
//    
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    customViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"newCustomView"];
//    
//    [[self navigationController] pushViewController:vc animated:true];
//    //vc.pID=path.row;
//}



@end
