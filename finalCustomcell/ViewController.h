//
//  ViewController.h
//  finalCustomcell
//
//  Created by Taslima Roya on 9/19/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table;


@end

