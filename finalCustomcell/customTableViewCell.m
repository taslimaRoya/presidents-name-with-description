//
//  customTableViewCell.m
//  finalCustomcell
//
//  Created by Taslima Roya on 9/19/18.
//  Copyright © 2018 Taslima Roya. All rights reserved.
//

#import "customTableViewCell.h"

@implementation customTableViewCell
@synthesize label1=_label1;
@synthesize label2=_label2;
@synthesize image=_image;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
